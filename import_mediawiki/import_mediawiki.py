#!/usr/bin/env python
import requests
from lxml import html, etree
import re
import subprocess
import urllib

def enumerate_all_page_names(wiki, domain):
    url = wiki + "/Special:AllPages"
    page = requests.get(url)
    tree = html.fromstring(page.content)
    links = tree.xpath('//a/@href')
    regexp = re.compile(r'^/{}/(.*)$'.format(re.escape(domain)))
    matched_links = (re.match(regexp, address) for address in links)
    page_names = [groups[1] for groups in matched_links if groups != None]
    return set(page_names)

def get_page_tree(wiki, page_name):
    url = wiki + "/Special:Export/{}".format(page_name)
    page = requests.get(url)
    tree = etree.fromstring(page.content)
    return tree

def pandoc(text, input_format, output_format):
    command = ["pandoc", "-f", input_format, "-t", output_format]
    process = subprocess.Popen(command,
        stdin = subprocess.PIPE, stdout = subprocess.PIPE)
    process.stdin.write(text.encode())
    process.stdin.close()
    result = process.stdout.read().decode()
    process.wait()
    return result

def download_file(wiki, filename):
    url = "{}/File:{}".format(wiki, filename)
    page = requests.get(url)
    tree = html.fromstring(page.content)
    print(filename)
    path = tree.xpath('//div[@class="fullImageLink"]/a/@href'.format(filename))
    if path == []:
        path = tree.xpath('//span[@class="dangerousLink"]/a/@href'.format(filename))
        target_filename = "static/res/" + filename
    else:
        target_filename = "static/img/" + filename
    path = path[0]
    url = urllib.parse.urljoin(wiki, path)
    response = requests.get(url)
    with open(target_filename, "wb") as f:
        f.write(response.content)

def generate_hugo_page(wiki, page_name, tree, is_main_page):
    mediawiki_ns = "http://www.mediawiki.org/xml/export-0.10/"
    ns = { "m" : mediawiki_ns }
    title = tree.xpath('/m:mediawiki/m:page/m:title/text()', namespaces = ns)
    if title == []:
        return
    title = title[0]
    print(title)
    text = tree.xpath('/m:mediawiki/m:page/m:revision/m:text/text()', namespaces = ns)
    text = str(text[0])
    for filename in re.findall('File: *([^\]| ][^\]|]*[^\]| ]) *(?:]|\|)', text):
        download_file(wiki, filename)
    def replace(match):
        # External links: [...]
        if match[1] != None:
            return '[{}]'.format(re.sub(r"([^ ])('''.)", r'\1 \2', match[1]))
        # Internal links: [[...]]
        if match[2] != None:
            args = match[2].split('|')
            filename = re.match(r'File: *([^ ]*) *', args[0])
            if filename != None:
                filename = filename[1]
                if filename.endswith('.png'):
                    return '[[File:/doc-ci/img/{}]]'.format(filename)
                else:
                    return '[[File:/doc-ci/res/{}]]'.format(filename)
            url = args[0].lower()
            if not(url.startswith('#')):
                if is_main_page:
                    url = 'page/' + url
                else:
                    url = '../' + url
            parts = url.split('#')
            if len(parts) == 2:
                url = parts[0] + '#' + re.sub(r'_| ', '-', parts[1])
            if len(args) == 1:
                return '[[{}]]'.format(url)
            else:
                return '[[{}|{}]]'.format(url, args[1])
        # <gallery>
        if match[3] != None:
            images = match[3].split('\n')
            lines = []
            for image in images:
                args = image.split('|')
                filename = re.match(r'File: *([^ ]*) *', args[0])
                if filename != None:
                    lines.append('# [[File:/doc-ci/img/{}|{}]]'.format(filename[1], args[1]))
            return '\n'.join(lines)
    text = re.sub(r'\[([^[\]]*)\]|\[\[([^\]]*)\]]|<gallery>([^<]*)</gallery>', replace, text);
    with open("content/page/" + page_name + ".mw", "w") as f:
        f.write(text)
    text = pandoc(text, "mediawiki", "markdown")
    text = re.sub(r"\\('|\x22|“|”|@)", r"\1", text)
    if is_main_page:
        filename = "content/_index.md"
    else:
        filename = "content/page/{}.md".format(page_name)
    if is_main_page:
        title = site_title
    with open(filename, "w") as f:
        f.write("---\n")
        f.write("title: \"{}\"\n".format(title))
        f.write("---\n")
        f.write(text)

wiki = "https://wiki.inria.fr/ciportal"
domain = "ciportal"
site_title = "CI documentation"

for page_name in enumerate_all_page_names(wiki, domain):
    generate_hugo_page(wiki, page_name, get_page_tree(wiki, page_name), page_name == "Main_Page")
    
