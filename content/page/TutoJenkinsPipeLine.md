---
title: "TutoJenkinsPipeLine"
---
Jenkins Pipeline
================

Since the version 2.x of Jenkins, the user which creates jobs or
"items" could use **pipelines** to organize the continuous integration
(CI) process. This way enables the user to store the quasi-entire
configuration of CI in a **JenkinsFile** who can be store on the SCM
repository. Thus, there is anymore need to make tedious configuration by
clicking. In this tutorial we will present a basic usage of **Jenkins
pipelines**

`- retrieving the master branch`\
`- compiling the sources`\
`- testing the result`

Configuration
-------------

You will need to install the following plugins and we consider that you
Jenkins version is up to 2.x (in reality, we tried with Jenkins 2.138.1)

`- Git plugin`\
`- Declarative pipeline`\
`- xUnit Plugin`

Retrieving the sources
----------------------

We will take the example of a private project on the gitlab forge.

### Add key

`- First, retrieve the public key of your project, go to  `[`https://ci.inria.fr/project/your_project/slaves`](https://ci.inria.fr/project/your_project/slaves)

![](/doc/img/RetrieveKey.png "/doc/img/RetrieveKey.png")

`- On Gitlab, go to project/settings/repository and `**`expand`**` the deploy keys section, copy-paste`\
`  the content of the previous download file and add your key (tick write allow)`

### create a new project

`- Connect to Jenkins (http://ci.inria.fr/your_project/)  and select `**`New`` ``Item`**\
`- select `**`multibranch`` ``Pipeline`**` and choose a name`\
`- in the `**`branch`` ``sources`**` select Git, put`\
` - Repository → git@gitlab.inria.fr:YOUR_USER/YOUR_PROJECT.git`\
` - Credentials → ci`

[800px](../file:branch_sources.png "wikilink")

`- Click on `**`Save`**

Compiling the sources
---------------------

A pipeline script is something like that <code>

pipeline {

`  agent any`\
`      stages {`\
`          stage('Build') {`\
`              steps {`\
`                  //`\
`              }`\
`          }`\
`          stage('Test') {`\
`              steps {`\
`                  //`\
`              }`\
`          }`\
`          }`\
`      }`\
`   }`

</code> It's a DSL (domain specific language) based on
[Groovy](https://en.wikipedia.org/wiki/Apache_Groovy) language. It
describes all the steps that form the **pipeline**. You can put that
file on the repository as a JenkinsFile. - For starting , assume we have
a WCmakeLists.txt at the root of our project, put the following code
into a **Jenkinsfile** at the root of your project <code>

pipeline {

`   agent any`\
`       stages {`\
`           stage('Configure') {`\
`               steps {`\
`                   echo 'Configuring..'`\
`                   sh 'mkdir -p build && cd build && cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo ..'`\
`               }`\
`           }`\
`           stage('Build') {`\
`               steps {`\
`                   echo 'Building..'`\
`                   sh 'cd build && make'`\
`               }`\
`           }`

}

</code> the name of the **Jenkinsfile** is what you put in the field
