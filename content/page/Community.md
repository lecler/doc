---
title: "Community"
---
![](/doc/img/Community.png "/doc/img/Community.png")

Users are welcomed to share their knowledge related to:

-   general concepts and good practices about continuous integration
-   advanced usage of Jenkins
-   creation of slave template with non-featured operating systems, see
    [this page](../create_community_slaves "wikilink") for more
    information.
-   etc

A good idea is to:

1.  subscribe to the dedicated mailing-list:
    [ci-community](https://sympa.inria.fr/sympa/info/ci-community)
2.  send a message to *ci-community@inria.fr* when you want to share a
    useful experience

If you want to write a specific documentation or modify the existing one,
documentation can be found in the
[following public gitlab repository](https://gitlab.inria.fr/inria-ci/doc).
Do not hesitate to send us pull requests or to create
[documentation issues](https://gitlab.inria.fr/inria-ci/doc/issues).
